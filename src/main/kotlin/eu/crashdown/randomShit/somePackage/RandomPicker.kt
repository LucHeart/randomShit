package eu.crashdown.randomShit.somePackage

import java.util.*

/**
 * @author zLucPlayZ
 * @cpackage eu.crashdown.countdown.app
 * @cdate 13.08.2018
 * @ctime 02:32
 * @cproject Countdown
*/

object RandomPicker {

    fun pickRandom(): String = Choices.values()[(0..Choices.values().size).random()].name

    private fun ClosedRange<Int>.random() =
            Random().nextInt((endInclusive) - start) +  start

}